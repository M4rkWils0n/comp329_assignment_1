import lejos.hardware.Button;
import lejos.robotics.subsumption.Arbitrator;

public class InitialClass
{

	public static void main(String[] args)
	{
		Arena arena = new Arena();

		PilotRobot robot = new PilotRobot(arena);

		BoundaryCheckBehaviour firstRun = new BoundaryCheckBehaviour(robot, arena);

		// Creates PilotMonitor
		PilotMonitor myMonitor = new PilotMonitor(robot, arena, 400);

		myMonitor.start();

		// Sets up Arbitrator for initial exploration around the edge of the arena
		Arbitrator firstRunArby = firstRun.setUpArbitrator();

		// Tell the user to start
		myMonitor.setMessage("Press a key to start");
		Button.waitForAnyPress();

		// Initial scan before behaviour starts
		arena.updateArenaInfo(robot.scanAround());
		robot.setOccupied(arena.isFrontOccupied(), arena.isLeftOccupied(), arena.isRightOccupied());

		firstRunArby.go();
	}
}
