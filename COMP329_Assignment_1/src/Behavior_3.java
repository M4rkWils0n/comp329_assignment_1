import lejos.robotics.subsumption.Behavior;

//Outline of class structure and used methods for the Behaviours
//Should rename class to behaviour name
public class Behavior_3 implements Behavior
{

	public boolean suppressed;
	private PilotRobot me;
	

	// Constructor - the robot, and gets access to the pilot class
	// that is managed by the robot (this saves us calling
	// me.getPilot.somemethod() all of the while)
    public Behavior_3(PilotRobot robot)
    {
    		me = robot;
    }

	// When called, this should stop action()
	public void suppress()
	{
		suppressed = true;
	}
	
	// When called, determine if this behaviour should start
	public boolean takeControl()
	{
			return true;
	}

	// This is our action function.  All calls to the motors should be
	// non blocking, so that they can be stopped if suppress is true.
	// If a call is made to move a specific distance or rotate a specific
	// angle etc, then it should return immediately, and monitored until
	// it has completed.  The code below illustrates this, but waiting
	// until the robot stops moving.  An OdometryPoseProvider could also
	// be used for this.
	
	public void action() 
	{
			// Allow this method to run
			suppressed = false;
			
			// Go Forward
			me.getPilot().forward();
			
			// While we can run, yield the thread to let other threads run.
			// It is important that no action function blocks any other action.
			while (!suppressed) 
			{
				Thread.yield();
			}
			
		    // Ensure that the motors have stopped.
			me.getPilot().stop();
	}
}
