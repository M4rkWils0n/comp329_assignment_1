import lejos.hardware.Button;
import lejos.hardware.Keys;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.motor.Motor;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;

/**
 * Class to configure the MovePilot
 * @author markwilson
 *
 */
public class Configurator 
{
	MovePilot pilot;
	GraphicsLCD lcd;

	public double leftWheelOffset = -10.0;
	public double rightWheelOffset = 10.0;
	
	// Here's the main part of the program, a function that repeats two calls
	// to the pilot.
	public void  drawSquare(float length)
	{   
		for(int i = 0; i<4 ; i++)
		{
			pilot.travel(length);       // Drive forward
			pilot.rotate(90);           // Turn 90 degrees    
		}
	}
        
	public void reduceOffset(String side)
	{
		if(side == "left")
		{
			leftWheelOffset = leftWheelOffset - 0.1;
		}
		else if(side == "right")
    		{
    			rightWheelOffset = rightWheelOffset - 0.1;
    		}
    		
    		this.configureChassis();
	}
    
	public void increaseOffset(String side)
	{
		if(side == "left")
		{
			leftWheelOffset = leftWheelOffset + 0.1;
		}
		else if(side == "right")
		{
			rightWheelOffset = rightWheelOffset + 0.1;
		}
	    	
	    	this.configureChassis();
	}	
    
	public void introMessage() 
	{	
		lcd.clear();
		lcd.setFont(Font.getDefaultFont());
		lcd.drawString("Configurator", lcd.getWidth()/2, 0, GraphicsLCD.HCENTER);
		lcd.setFont(Font.getSmallFont());
 
		lcd.drawString("This allows you to  ", 0, 20, 0);
		lcd.drawString("configure the MovePilot", 0, 30, 0);
		lcd.drawString("Up: Configure Left Offset", 0, 50, 0);
		lcd.drawString("Down: Configure Right Offset ", 0, 60, 0);
		lcd.drawString("ENTER Button: Draw Square ", 0, 70, 0);
		lcd.drawString("ESC Button: Quit and Save ", 0, 80, 0);
	}

	public void configureChassis()
	{
    		Wheel leftWheel = WheeledChassis.modelWheel(Motor.B, 3.3).offset(this.leftWheelOffset);
		Wheel rightWheel = WheeledChassis.modelWheel(Motor.C, 3.3).offset(this.rightWheelOffset);
		
		Chassis myChassis = new WheeledChassis
				( 
						new Wheel[]{ leftWheel, rightWheel }, WheeledChassis.TYPE_DIFFERENTIAL
				);
		
		this.pilot = new MovePilot(myChassis);
	}
    
	public static void main(String[] args) 
	{	
		// Create a SimplePilot and instantiate its member pilot
		Configurator config = new Configurator();
		
		// Set up the wheels by specifying the diameter of the
		// left (and right) wheels in centimetres, i.e. 3.25 c.m
		// the offset number is the distance between the centre
		// of wheel to the centre of robot, i.e. half of track width
		// NOTE: this may require some trial and error to get right!!!
		config.configureChassis();
	
		config.lcd = LocalEV3.get().getGraphicsLCD();
	
		config.pilot.setLinearSpeed(20); 	// Set speed to 10cm per second
	
		int keyPressed = Button.getButtons();
	
		int offsetSelection = 0;
		boolean doesShowIntroMessage = true;
	
		final int leftSelection = 1;
		final int rightSelection = 2;
		final int resetSelection = 0;
		
		// Print a message on the screen and wait for one of the button presses        
		while (Button.getButtons() != Keys.ID_ESCAPE) 
		{  
			if(doesShowIntroMessage)
	        		{
	    			config.lcd.clear();
	        			config.introMessage();
	        			doesShowIntroMessage = false;
	        			Button.waitForAnyPress(); 
	        			keyPressed = Button.getButtons();
	        		}
	        		
	    		switch (keyPressed) 
	    		{
		    		case Keys.ID_UP:			// Select Left wheel configuration
		    			config.lcd.clear();
					String offsetLeft = Double.toString(config.leftWheelOffset);
					config.lcd.drawString("Configure the left wheel", 0, 10, 0);
					config.lcd.drawString("Current offset is: " + offsetLeft , 0, 30, 0);
					offsetSelection = leftSelection;
		    			break;
		            		
	            		case Keys.ID_DOWN:		// Select Right wheel configuration	
	            			config.lcd.clear();
	       				String offsetRight = Double.toString(config.rightWheelOffset);
	       				config.lcd.drawString("Configure the right wheel", 0, 10, 0);
	       				config.lcd.drawString("Current offset is: " + offsetRight , 0, 30, 0);
	           			offsetSelection = rightSelection;
	        				break;
	            			
	            		case Keys.ID_LEFT:		// After offset is selected then decrease selected side
	            			
	            			if( offsetSelection == leftSelection )
	            			{
	            				config.lcd.clear();
	            				config.lcd.drawString("Configure the left wheel", 0, 10, 0);
	            				config.reduceOffset("left");
	            				String offsetLeft1 = Double.toString(config.leftWheelOffset);
	            				config.lcd.drawString("Current offset is: " + offsetLeft1 , 0, 30, 0);	
	            			}
	            			else if( offsetSelection == rightSelection )
	            			{
	            				config.lcd.clear();
	            				config.lcd.drawString("Configure the right wheel", 0, 10, 0);
	            				config.reduceOffset("right");
	            				String offsetLeft1 = Double.toString(config.rightWheelOffset);
	            				config.lcd.drawString("Current offset is: " + offsetLeft1 , 0, 30, 0);
	            			}
	            			else
	            			{
	            				config.lcd.clear();
	            				config.lcd.drawString("Please select a wheel", 0, 10, 0);
	            			}
	        				break;
	            		
	            		case Keys.ID_RIGHT:		// After offset is selected then increase selected side
	            			if( offsetSelection == leftSelection )
	            			{
	            				config.lcd.clear();
	            				config.lcd.drawString("Configure the left wheel", 0, 10, 0);
	            				config.increaseOffset("left");
	            				String offsetLeft1 = Double.toString(config.leftWheelOffset);
	            				config.lcd.drawString("Current offset is: " + offsetLeft1 , 0, 30, 0);	
	            			}
	            			else if( offsetSelection == rightSelection )
	            			{
	            				config.lcd.clear();
	            				config.lcd.drawString("Configure the right wheel", 0, 10, 0);
	            				config.increaseOffset("right");
	            				String offsetLeft1 = Double.toString(config.rightWheelOffset);
	            				config.lcd.drawString("Current offset is: " + offsetLeft1 , 0, 30, 0);
	            			}
	            			else
	            			{
	            				config.lcd.clear();
	            				config.lcd.drawString("Please select a wheel", 0, 10, 0);
	            				// THis is a test
	            			}
	        				break;
	            		
	            		case Keys.ID_ENTER:			// Call drawSquare with the length of the side.
	            			config.lcd.clear();
	            			config.lcd.drawString("Drawing Square", 0, 10, 0);
	        				config.drawSquare(40); 
	        				config.configureChassis();
	        				offsetSelection = resetSelection;
	        				doesShowIntroMessage = true;
	        				break;
	            	
	            		default:
	        				config.lcd.drawString("Keys: left right up enter", 0, 120, 0);
	        				break;    			
		            }
	    		
	    		Button.waitForAnyPress(); 
	    		keyPressed = Button.getButtons();
	        }
	
	        String path = "config.txt";
	        String [] chassisConfig = {Double.toString(config.leftWheelOffset), Double.toString(config.rightWheelOffset)};
	        
	        WriteToFile itemsToWrite = new WriteToFile(chassisConfig, path);
	        
	        if(itemsToWrite.writeToFile())
	        {
	        		config.lcd.clear();
	        		config.lcd.drawString("Offsets Written to disk", 0, 10, 0);
	        		config.lcd.drawString("Left: " + Double.toString(config.leftWheelOffset), 0, 30, 0);
	        		config.lcd.drawString("RIght: " +  Double.toString(config.rightWheelOffset), 0, 40, 0);
	        }
	        else
	        {
	        		config.lcd.clear();
	        		config.lcd.drawString("Items did not write", 0, 10, 0);
	        }
	}									
}
