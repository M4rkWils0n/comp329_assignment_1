public class Arena
{

	public static final double ARENA_WIDTH = 150; // old 60 inch 152.4
	public static final double ARENA_DEPTH = 195; // old 82 inch 208.28
	public static final int CELL_WIDTH = 4; // old 6
	public static final int CELL_DEPTH = 5; // old 7
	public static final double UNIT_WIDTH = ARENA_WIDTH / CELL_WIDTH;
	public static final double UNIT_DEPTH = ARENA_DEPTH / CELL_DEPTH;

	public static final double BIAS = 10;
	public static final double THRESHOLD = 0.55;

	private double[][] arena = new double[CELL_WIDTH + 2][CELL_DEPTH + 2];
	private int[][] occupy = new int[CELL_WIDTH + 2][CELL_DEPTH + 2];
	private int[][] count = new int[CELL_WIDTH + 2][CELL_DEPTH + 2];

	private int[] position = new int[] { 1, 1 }; // init [1, 1]
	private int[] direction = new int[] { 0, 1 }; // init N [0, 1]
	private int[] destination = new int[] { 0, 0 };

	public Arena()
	{
	}

	public void setArena(int[] location, double value)
	{
		arena[location[0]][location[1]] = value;
	}

	public void setArena(int[] location)
	{
		setArena(location, getProbability(location));
	}

	public void setArena(double[][] arena)
	{
		this.arena = arena;
	}

	public void setOccupy(int[] location, boolean occupied)
	{
		if (occupied)
		{
			occupy[location[0]][location[1]]++;
		} else
		{
			occupy[location[0]][location[1]]--;
		}
	}

	public void setOccupy(int[][] occupy)
	{
		this.occupy = occupy;
	}

	public void setCount(int[] location)
	{
		count[location[0]][location[1]]++;
	}

	public void setCount(int[][] count)
	{
		this.count = count;
	}

	public void setPosition(int[] position)
	{
		this.position = position;
	}

	public void setDirection(int[] direction)
	{
		this.direction = direction;
	}

	public void setDestination(int[] destination)
	{
		this.destination = destination;
	}

	public double getProbability(int[] location)
	{
		int occupyNum = occupy[location[0]][location[1]];
		int countNum = count[location[0]][location[1]];
		return (occupyNum + countNum) / (2.0 * countNum);

	}

	public double[][] getArena()
	{
		return arena;
	}

	public int[][] getOccupy()
	{
		return occupy;
	}

	public int[][] getCount()
	{
		return count;
	}

	public int[] getPosition()
	{
		return position;
	}

	public int[] getDirection()
	{
		return direction;
	}

	public int[] getDestination()
	{
		return destination;
	}

	// TODO Cong if occupied true else false
	public boolean isLeftOccupied()
	{
		int[] pos = getPosition();
		int[] dir = getDirection();

		if (getArena()[pos[0] - dir[1]][pos[1] + dir[0]] <= Arena.THRESHOLD)
		{
			return false;
		}
		return true;
	}

	// TODO Cong if occupied true else false
	public boolean isFrontOccupied()
	{
		int[] pos = getPosition();
		int[] dir = getDirection();

		if (getArena()[pos[0] + dir[0]][pos[1] + dir[1]] <= Arena.THRESHOLD)
		{
			return false;
		}
		return true;
	}

	// TODO Cong if occupied true else false
	public boolean isRightOccupied()
	{
		int[] pos = getPosition();
		int[] dir = getDirection();

		if (getArena()[pos[0] + dir[1]][pos[1] - dir[0]] <= Arena.THRESHOLD)
		{
			return false;
		}
		return true;
	}

	public void setDirectionFromOpp(float direction)
	{
		int[] dir = getDirection();

		if (direction == 90)
		{
			setDirection(new int[] { -dir[1], dir[0] });
		} else if (direction == -90)
		{
			setDirection(new int[] { dir[1], -dir[0] });
		} else if (direction == 180)
		{
			setDirection(new int[] { -dir[0], -dir[1] });
		}
	}

	public void updateArenaInfo(float[] data)
	{
		int[] pos = getPosition();
		int[] dir = getDirection();
		int[] cells = new int[3];
		cells[0] = (int) Math.ceil(data[0] / (Math.abs(dir[1]) * UNIT_WIDTH + Math.abs(dir[0]) * UNIT_DEPTH));
		cells[1] = (int) Math.ceil(data[1] / (Math.abs(dir[1]) * UNIT_WIDTH + Math.abs(dir[0]) * UNIT_DEPTH));
		cells[2] = (int) Math.ceil(data[2] / (Math.abs(dir[0]) * UNIT_WIDTH + Math.abs(dir[1]) * UNIT_DEPTH));
		for (int i = 1; i <= cells[0]; i++)
		{ // left
			int[] location = new int[] { pos[0] - dir[1] * i, pos[1] + dir[0] * i };
			setCount(location);
			setOccupy(location, i == cells[0]);
			setArena(location);
		}
		for (int i = 1; i <= cells[1]; i++)
		{ // right
			int[] location = new int[] { pos[0] + dir[1] * i, pos[1] - dir[0] * i };
			setCount(location);
			setOccupy(location, i == cells[1]);
			setArena(location);
		}
		for (int i = 1; i <= cells[2]; i++)
		{ // front
			int[] location = new int[] { pos[0] + dir[0] * i, pos[1] + dir[1] * i };
			setCount(location);
			setOccupy(location, i == cells[2]);
			setArena(location);
		}

	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder("");
		for (int i = 0; i < arena.length; i++)
		{
			for (int j = 0; j < arena[i].length; j++)
			{
				sb.append(arena[i][j]);
				sb.append(" ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}

}
