/**
 * Class to read from file
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromFile
{
	private final String FILENAME ;
	private String[] retrievedData;
	private int retrievalAmount;

	/**
	 * Pass file name to read and amount of lines expected
	 * @param fileName - name of File
	 * @param retrievalAmount - items to be retrieved
	 */
	public ReadFromFile(String fileName, int retrievalAmount)
	{
		this.FILENAME = fileName;
		this.retrievalAmount = retrievalAmount;
		retrievedData = new String[this.retrievalAmount];
		getDataFromFile();
	}
	
	/**
	 * Get data from File
	 */
	private void getDataFromFile()
	{
		BufferedReader br = null;
		FileReader fr = null;
		
		try {
			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);

			String sCurrentLine;

			int increment = 0;
			
			while ((sCurrentLine = br.readLine()) != null) 
			{
				if(increment < retrievalAmount)
				{
					retrievedData[increment++] = sCurrentLine;	
				}	
			}
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			try 
			{
				if (br != null) 
				{
					br.close();
				}
				if (fr != null) 
				{
					fr.close();
				}
			} 
			catch (IOException ex) 
			{
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * @return String array, each position a new line of retrieved data
	 */
	public String[] getData()
	{
		return retrievedData;
	}
}

