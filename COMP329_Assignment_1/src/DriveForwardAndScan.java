import lejos.robotics.subsumption.Behavior;

//Outline of class structure and used methods for the Behaviours
//Should rename class to behaviour name
public class DriveForwardAndScan implements Behavior
{
	public boolean suppressed;
	private PilotRobot robot;

	private Arena arena;
	private float[] scanData;

	public DriveForwardAndScan(PilotRobot robot, Arena arena)
	{
		this.robot = robot;
		this.arena = arena;
	}

	// When called, determine if this behaviour should start
	public boolean takeControl()
	{
		return true;
	}

	// This is our action function. All calls to the motors should be
	// non blocking, so that they can be stopped if suppress is true.
	// If a call is made to move a specific distance or rotate a specific
	// angle etc, then it should return immediately, and monitored until
	// it has completed. The code below illustrates this, but waiting
	// until the robot stops moving. An OdometryPoseProvider could also
	// be used for this.

	public void action()
	{
		// Allow this method to run
		suppressed = false;

		// Go Forward a unit
		robot.moveOneUnit(arena.getDirection());

		robot.setArenaPositionAfterMove(arena.getDirection(), arena.getPosition());

		// While we can run, yield the thread to let other threads run.
		// It is important that no action function blocks any other action.
		while (!suppressed && robot.getPilot().isMoving())
		{
			Thread.yield();
		}

		if (!suppressed)
		{
			scanData = robot.scanAround();
			arena.updateArenaInfo(scanData);

			robot.setOccupied(arena.isFrontOccupied(), arena.isLeftOccupied(), arena.isRightOccupied());
		}
	}

	// When called, this should stop action()
	public void suppress()
	{
		suppressed = true;
	}
}