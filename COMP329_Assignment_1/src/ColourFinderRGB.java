import lejos.hardware.Brick;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;

public class ColourFinderRGB
{
	private Brick myEV3;
	private EV3ColorSensor cSensor;
	private SampleProvider colourSP;
	private float[] colourSample; 
	
	public ColourFinderRGB(Brick myEV3)
	{
		this.myEV3 = myEV3;
		cSensor = new EV3ColorSensor(this.myEV3.getPort("S4"));
		colourSP = cSensor.getRGBMode();
		
		// Sets the Float array size
		colourSample = new float[colourSP.sampleSize()];	
	}
	
	// Gets the String of the Current Colour
	public String getCurrentColour()
	{
		String currentColour = "No Colour";
		
		colourSP.fetchSample(colourSample, 0);
		
		float redRGB = colourSample[0];
		
		if(redRGB > 0.02 && redRGB < 0.32)
		{
			currentColour = "White";
		}
		else if(redRGB < 0.02)
		{
			currentColour = "Blue";
		}
		
		return currentColour;
	}
	
	public void closeSensor()
	{
		cSensor.close();
	}
}
