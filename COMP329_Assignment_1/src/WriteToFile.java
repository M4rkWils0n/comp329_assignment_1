import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class WriteToFile
{
	private String[] itemsToWrite;
	private String path;
	
	public WriteToFile(String[] array, String path)
	{
		int lengthOfArray = array.length;
		this.itemsToWrite = new String[lengthOfArray];
		this.path = path;
		
		for(int i = 0; i < lengthOfArray; i++ )
		{
			itemsToWrite[i] = array[i];
		}	
	}
	
	public boolean writeToFile()
	{
		List<String> lines = Arrays.asList(itemsToWrite);
		Path file = Paths.get(path);
	        try 
	        {
	        		Files.write(file, lines, Charset.forName("UTF-8"));
	        		return true;
		} 
	        catch (IOException e) 
	        {
	        		// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
	        }   
	}
}
