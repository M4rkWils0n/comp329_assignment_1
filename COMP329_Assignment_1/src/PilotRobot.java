import java.util.Arrays;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.motor.Motor;
// import lejos.hardware.sensor.EV3IRSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.localization.OdometryPoseProvider;
import lejos.robotics.navigation.MovePilot;

public class PilotRobot
{
	private EV3TouchSensor leftBump, rightBump;
	private EV3UltrasonicSensor irSensor;
	// private NXTUltrasonicSensor irSensor;
	private ColourFinderRGB colourSensor;
	private SampleProvider leftSP, rightSP, distSP;
	private float[] leftSample, rightSample, distSample;
	private MovePilot pilot;
	private OdometryPoseProvider opp;

	private boolean leftOccupied = false;
	private boolean rightOccupied = false;
	private boolean frontOccupied = false;

	private Arena arena;

	public PilotRobot(Arena arena)
	{
		// Gives pilot arena knowledge
		this.arena = arena;

		// TODO: Need to set up the NXT sonicSensor instead of the ir sensor
		Brick myEV3 = BrickFinder.getDefault();

		// Creates an instance of the touch sensors left and right
		leftBump = new EV3TouchSensor(myEV3.getPort("S2"));
		rightBump = new EV3TouchSensor(myEV3.getPort("S1"));

		// Creates an instance of the IR sensor
		// irSensor = new EV3IRSensor(myEV3.getPort("S3"));
		irSensor = new EV3UltrasonicSensor(myEV3.getPort("S3"));
		// irSensor = new NXTUltrasonicSensor(myEV3.getPort("S3"));
		// Creates an instance of the colour sensor
		colourSensor = new ColourFinderRGB(myEV3);

		// Adds modes used of the sensors
		leftSP = leftBump.getTouchMode();
		rightSP = rightBump.getTouchMode();
		distSP = irSensor.getDistanceMode();

		// Sample Size
		leftSample = new float[leftSP.sampleSize()];
		rightSample = new float[rightSP.sampleSize()];
		distSample = new float[distSP.sampleSize()];

		// Retrieve left and right Wheel data from .config file
		String fileName = "config.txt";
		int itemsToBeRetrieved = 2;
		String[] retrievedData = new String[itemsToBeRetrieved];
		float leftOffset;
		float rightOffset;

		ReadFromFile fileData = new ReadFromFile(fileName, itemsToBeRetrieved);
		retrievedData = fileData.getData();
		leftOffset = Float.parseFloat(retrievedData[0]);
		rightOffset = Float.parseFloat(retrievedData[1]);

		// Set up the wheels by specifying the diameter of the
		// left (and right) wheels in centimetres, i.e. 3.25 c.m
		Wheel leftWheel = WheeledChassis.modelWheel(Motor.B, 3.3).offset(leftOffset);
		Wheel rightWheel = WheeledChassis.modelWheel(Motor.C, 3.3).offset(rightOffset);
		Chassis myChassis = new WheeledChassis(new Wheel[] { leftWheel, rightWheel },
				WheeledChassis.TYPE_DIFFERENTIAL);

		// Adds the chassis to the move pilot
		pilot = new MovePilot(myChassis);

		opp = new OdometryPoseProvider(pilot);
	}

	// Closes the sensors
	public void closeRobot()
	{
		leftBump.close();
		rightBump.close();
		irSensor.close();
		colourSensor.closeSensor();
	}

	// Method called when Left bumper is pressed
	public boolean isLeftBumpPressed()
	{
		leftSP.fetchSample(leftSample, 0);
		return (leftSample[0] == 1.0);
	}

	// Method called when Right bumper is pressed
	public boolean isRightBumpPressed()
	{
		rightSP.fetchSample(rightSample, 0);
		return (rightSample[0] == 1.0);
	}

	// Gets the distance from the IR Sensor
	public float getDistance()
	{
		distSP.fetchSample(distSample, 0);
		return distSample[0];
	}

	// Gets the colourSample from the colour sensor
	public String getColour()
	{
		return colourSensor.getCurrentColour();
	}

	// Get the OdemetryPoseProvider
	public OdometryPoseProvider getOdometry()
	{
		return opp;
	}

	// Gets the current Pilot
	public MovePilot getPilot()
	{
		return pilot;
	}

	public void setOccupied(Boolean front, Boolean left, Boolean right)
	{
		this.frontOccupied = front;
		this.leftOccupied = left;
		this.rightOccupied = right;
	}

	public boolean isLeftOccupied()
	{
		return this.leftOccupied;
	}

	public boolean isRightOccupied()
	{
		return this.rightOccupied;
	}

	public boolean isFrontOccupied()
	{
		return this.frontOccupied;
	}

	// Cong moved methods
	/**
	 * Scan for distance.
	 *
	 * @param total
	 *                number of scans
	 * @param sample
	 *                number of sample
	 * @param delay
	 *                delay between each scan
	 * @return mean of samples
	 */
	public float scan(int total, int sample, int delay)
	{
		float[] data = new float[total];
		for (int i = 0; i < total; i++)
		{
			data[i] = getDistance();
			try
			{
				Thread.sleep(delay);
			} catch (InterruptedException e)
			{
			}
		}
		Arrays.sort(data);
		float sum = 0;
		int diff = total - sample;
		int min = diff / 2;
		int max = diff % 2 == 0 ? total - min : total - min - 1;
		for (int i = min; i < max; i++)
		{
			sum += data[i];
		}
		return sum / sample;
	}

	/**
	 * Turn around and scan.
	 *
	 * @return distance of [left, right, front]
	 */
	public float[] scanAround()
	{
		float[] dis = new float[3];
		Motor.A.rotate(90);
		dis[0] = scan(5, 3, 100);
		Motor.A.rotate(-180);
		dis[1] = scan(5, 3, 100);
		Motor.A.rotate(90);
		dis[2] = scan(5, 3, 100);
		return dis;
	}

	public void moveOneUnit(int[] direction)
	{
		if (direction[0] == 0) // N [0, 1] & S [0, -1]
		{
			pilot.travel(Arena.UNIT_DEPTH, true);
		} else if (direction[1] == 0) // E [1, 0] & W [-1, 0]
		{
			pilot.travel(Arena.UNIT_WIDTH, true);
		}
	}

	public void setArenaPositionAfterMove(int[] direction, int[] position)
	{
		if (direction[0] == 0) // N [0, 1] & S [0, -1]
		{
			arena.setPosition(new int[] { position[0], position[1] + direction[1] });
		} else if (direction[1] == 0) // E [1, 0] & W [-1, 0]
		{
			arena.setPosition(new int[] { position[0] + direction[0], position[1] });
		}
	}

	public void determineDirection()
	{
		int[] pos = arena.getPosition();
		int[] dir = arena.getDirection();

		if (arena.getArena()[pos[0] - dir[1]][pos[1] + dir[0]] <= Arena.THRESHOLD)
		{ // left is empty
			pilot.rotate(90);
			arena.setDirection(new int[] { -dir[1], dir[0] });

		} else if (arena.getArena()[pos[0] + dir[0]][pos[1] + dir[1]] <= Arena.THRESHOLD)
		{ // front is empty
		} else if (arena.getArena()[pos[0] + dir[1]][pos[1] - dir[0]] <= Arena.THRESHOLD)
		{ // right is empty
			pilot.rotate(-90);
			arena.setDirection(new int[] { dir[1], -dir[0] });
		} else
		{
			pilot.rotate(180);
			arena.setDirection(new int[] { -dir[0], -dir[1] });
		}
	}

}
