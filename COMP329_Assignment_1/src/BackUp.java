import lejos.robotics.subsumption.Behavior;

//Outline of class structure and used methods for the Behaviours
//Should rename class to behaviour name
public class BackUp implements Behavior
{

	public boolean suppressed;
	private PilotRobot robot;
	private Arena arena;

	// Constructor - the robot, and gets access to the pilot class
	// that is managed by the robot (this saves us calling
	// me.getPilot.somemethod() all of the while)
	public BackUp(PilotRobot robot, Arena arena)
	{
		this.robot = robot;
		this.arena = arena;
	}

	// When called, determine if this behaviour should start
	public boolean takeControl()
	{
		if (robot.isLeftBumpPressed() || robot.isRightBumpPressed())
		{
			return true;
		}

		return false;
	}

	// This is our action function. All calls to the motors should be
	// non blocking, so that they can be stopped if suppress is true.
	// If a call is made to move a specific distance or rotate a specific
	// angle etc, then it should return immediately, and monitored until
	// it has completed. The code below illustrates this, but waiting
	// until the robot stops moving. An OdometryPoseProvider could also
	// be used for this.

	public void action()
	{
		// Allow this method to run
		suppressed = false;

		robot.getPilot().stop();

		// Reverse for 20cm, and have the thread yield until this is
		// complete (i.e. the robot stops) or if suppressed is true.
		robot.getPilot().travel(-10);

		// Rotate for 45 degrees, and have the thread yield until this is
		// complete (i.e. the robot stops) or if suppressed is true. Note
		// that we can check suppressed to see if it is even worth doing.
		// There are more elegant ways of doing this!!!
		robot.getPilot().rotate(90);

		robot.getPilot().travel(20, true);

		while (robot.getPilot().isMoving() && !suppressed)
		{
			Thread.yield();
		}

		// Ensure that the motors have stopped.
		robot.getPilot().stop();
	}

	// When called, this should stop action()
	public void suppress()
	{
		suppressed = true;
	}
}
