import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

public class BoundaryCheckBehaviour
{
	private PilotRobot robot;
	private Arena arena;

	public BoundaryCheckBehaviour(PilotRobot robot, Arena arena)
	{
		this.robot = robot;
		this.arena = arena;
		this.setUpArbitrator();
	}

	public Arbitrator setUpArbitrator()
	{
		Behavior driveAndScan = new DriveForwardAndScan(robot, arena);
		Behavior turnLeft = new TurnLeft(robot, arena);
		Behavior turnRight = new TurnRight(robot, arena);

		Behavior[] bArray = { driveAndScan, turnLeft, turnRight };
		Arbitrator arby = new Arbitrator(bArray);

		// Clear System print out from Arbitrator Message
		for (int i = 0; i < 8; i++)
		{
			System.out.println("");
		}

		return arby;
	}
}
