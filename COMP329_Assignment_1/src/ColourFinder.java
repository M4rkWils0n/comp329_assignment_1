import lejos.hardware.Brick;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.Color;

public class ColourFinder
{
	private Brick myEV3;
	private EV3ColorSensor cSensor;
	
	public ColourFinder(Brick myEV3)
	{
		this.myEV3 = myEV3;
		cSensor = new EV3ColorSensor(this.myEV3.getPort("S4"));
	}
	
	/**
	 * Method to Get Current Colour
	 * @return colour from sensor as a String
	 */
	public String getCurrentColour() 
	{
		String currentColour = "No Colour";
		
		switch(cSensor.getColorID())
		{
			case Color.BLUE:
		    		currentColour = "Blue";
				break;
			
			case Color.BLACK:
				currentColour = "Black";
				break;
			
			case Color.BROWN:
				currentColour = "Brown";
				break;
			
			case Color.CYAN:
				currentColour = "Cyan";
				break;
			
			case Color.DARK_GRAY:
				currentColour = "Dark_Gray";
				break;
				
			case Color.GRAY:
				currentColour = "Gray";
				break;
				
			case Color.GREEN:
				currentColour = "Green";
				break;
				
			case Color.LIGHT_GRAY:
				currentColour = "Light_Grey";
				break;
				
			case Color.MAGENTA:
				currentColour = "Magenta";
				break;
				
			case Color.ORANGE:
				currentColour = "Orange";
				break;
				
			case Color.NONE:
				currentColour = "None";
				break;
				
			case Color.PINK:
				currentColour = "Pink";
				break;
				
			case Color.RED:
				currentColour = "Red";
				break;
				
			case Color.WHITE:
				currentColour = "White";
				break;
				
			case Color.YELLOW:
				currentColour = "Yellow";
				break;
				
			default:
				currentColour = "Default";
				break;
		}

		return currentColour;
	}
}
