import lejos.robotics.subsumption.Behavior;

//Outline of class structure and used methods for the Behaviours
//Should rename class to behaviour name
public class TurnLeft implements Behavior
{
	public boolean suppressed;
	private PilotRobot robot;
	private Arena arena;
	private float[] scanData;

	public TurnLeft(PilotRobot robot, Arena arena)
	{
		this.robot = robot;
		this.arena = arena;
	}

	// When called, determine if this behaviour should start
	public boolean takeControl()
	{
		if (!robot.isLeftOccupied())
		{
			return true;
		}

		return false;
	}

	// This is our action function. All calls to the motors should be
	// non blocking, so that they can be stopped if suppress is true.
	// If a call is made to move a specific distance or rotate a specific
	// angle etc, then it should return immediately, and monitored until
	// it has completed. The code below illustrates this, but waiting
	// until the robot stops moving. An OdometryPoseProvider could also
	// be used for this.

	public void action()
	{
		// Allow this method to run
		suppressed = false;

		// Get direction before rotate
		int[] currentDirection = arena.getDirection();

		// Rotate
		robot.getPilot().rotate(-90, true);

		while (!suppressed && robot.getPilot().isMoving())
		{
			Thread.yield();
		}

		// Update direction after rotate
		arena.setDirection(new int[] { -currentDirection[1], currentDirection[0] });

		// Scan around
		scanData = robot.scanAround();

		// Update Arena Info
		arena.updateArenaInfo(scanData);

		// Set occupied
		robot.setOccupied(arena.isFrontOccupied(), arena.isLeftOccupied(), arena.isRightOccupied());
	}

	// When called, this should stop action()
	public void suppress()
	{
		suppressed = true;
	}
}
