import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.Motor;
import lejos.robotics.navigation.MovePilot;

public class Explorer
{
	private Arena arena;
	private PilotRobot robot;
	private MovePilot pilot;
	private PilotMonitor monitor;
	private DataOutputStream out;

	public Explorer() throws IOException
	{
		arena = new Arena();
		// robot = new PilotRobot();
		// pilot = robot.getPilot();
		// monitor = new PilotMonitor(robot, 400);
		// monitor.start();
		// monitor.setMessage("Press a key to start");
		new Thread(new Runnable() {
			@Override
			public void run()
			{
				try
				{
					ServerSocket server = new ServerSocket(10000);
					System.out.println("Awaiting client...");
					Socket client = server.accept();
					OutputStream os = client.getOutputStream();
					out = new DataOutputStream(os);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}).start();
	}

	public void writeInfo(String info)
	{
		try
		{
			out.writeUTF(info);
			out.flush();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Scan for distance.
	 *
	 * @param total
	 *                number of scans
	 * @param sample
	 *                number of sample
	 * @param delay
	 *                delay between each scan
	 * @return mean of samples
	 */
	public float scan(int total, int sample, int delay)
	{
		float[] data = new float[total];
		for (int i = 0; i < total; i++)
		{
			data[i] = robot.getDistance();
			try
			{
				Thread.sleep(delay);
			} catch (InterruptedException e)
			{
			}
		}
		Arrays.sort(data);
		float sum = 0;
		int diff = total - sample;
		int min = diff / 2;
		int max = diff % 2 == 0 ? total - min : total - min - 1;
		for (int i = min; i < max; i++)
		{
			sum += data[i];
		}
		return sum / sample;
	}

	/**
	 * Turn around and scan.
	 *
	 * @return distance of [left, right, front]
	 */
	public float[] scanAround()
	{
		float[] dis = new float[3];
		Motor.A.rotate(90);
		dis[0] = scan(5, 3, 100);
		Motor.A.rotate(-180);
		dis[1] = scan(5, 3, 100);
		Motor.A.rotate(90);
		dis[2] = scan(5, 3, 100);
		return dis;
	}

	/**
	 * Update info of arena.
	 *
	 * @param data
	 *                distances of [left, right, front]
	 * @param pos
	 *                position of robot
	 */
	public void updateArenaInfo(float[] data)
	{
		int[] pos = arena.getPosition();
		int[] direct = arena.getDirection();
		if (direct[0] == 0)
		{ // N [0, 1] & S [0, -1]
			int[] cells = new int[3];
			cells[0] = (int) Math.ceil(data[0] / Arena.UNIT_WIDTH); // left
			cells[1] = (int) Math.ceil(data[1] / Arena.UNIT_WIDTH); // right
			cells[2] = (int) Math.ceil(data[2] / Arena.UNIT_DEPTH); // front
			if (direct[1] == 1)
			{ // N [0, 1]
				// left
				for (int i = 1; i <= cells[0]; i++)
				{
					int[] location = new int[] { pos[0] - i, pos[1] }; // y fix, x decrease
					arena.setCount(location);
					arena.setOccupy(location, i == cells[0]);
					arena.setArena(location);
				}
				// right
				for (int i = 1; i <= cells[1]; i++)
				{
					int[] location = new int[] { pos[0] + i, pos[1] }; // y fix, x increase
					arena.setCount(location);
					arena.setOccupy(location, i == cells[1]);
					arena.setArena(location);
				}
				// front
				for (int i = 1; i <= cells[2]; i++)
				{
					int[] location = new int[] { pos[0], pos[1] + i }; // x fix, y increase
					arena.setCount(location);
					arena.setOccupy(location, i == cells[2]);
					arena.setArena(location);
				}
			} else if (direct[1] == -1)
			{ // S [0, -1]
				// left
				for (int i = 1; i <= cells[0]; i++)
				{
					int[] location = new int[] { pos[0] + i, pos[1] }; // y fix, x increase
					arena.setCount(location);
					arena.setOccupy(location, i == cells[0]);
					arena.setArena(location);
				}
				// right
				for (int i = 1; i <= cells[1]; i++)
				{
					int[] location = new int[] { pos[0] - i, pos[1] }; // y fix, x decrease
					arena.setCount(location);
					arena.setOccupy(location, i == cells[1]);
					arena.setArena(location);
				}
				// front
				for (int i = 1; i <= cells[2]; i++)
				{
					int[] location = new int[] { pos[0], pos[1] - i }; // x fix, y decrease
					arena.setCount(location);
					arena.setOccupy(location, i == cells[2]);
					arena.setArena(location);
				}
			}
		} else if (direct[1] == 0)
		{ // E [1, 0] & W [-1, 0]
			int[] cells = new int[3];
			cells[0] = (int) Math.ceil(data[0] / Arena.UNIT_DEPTH); // left
			cells[1] = (int) Math.ceil(data[1] / Arena.UNIT_DEPTH); // right
			cells[2] = (int) Math.ceil(data[2] / Arena.UNIT_WIDTH); // front
			if (direct[0] == 1)
			{ // E [1, 0]
				// left
				for (int i = 1; i <= cells[0]; i++)
				{
					int[] location = new int[] { pos[0], pos[1] + i }; // x fix, y increase
					arena.setCount(location);
					arena.setOccupy(location, i == cells[0]);
					arena.setArena(location);
				}
				// right
				for (int i = 1; i <= cells[1]; i++)
				{
					int[] location = new int[] { pos[0], pos[1] - i }; // x fix, y decrease
					arena.setCount(location);
					arena.setOccupy(location, i == cells[1]);
					arena.setArena(location);
				}
				// front
				for (int i = 1; i <= cells[2]; i++)
				{
					int[] location = new int[] { pos[0] + i, pos[1] }; // y fix, x increase
					arena.setCount(location);
					arena.setOccupy(location, i == cells[2]);
					arena.setArena(location);
				}
			} else if (direct[0] == -1)
			{ // W [-1, 0]
				// left
				for (int i = 1; i <= cells[0]; i++)
				{
					int[] location = new int[] { pos[0], pos[1] - i }; // x fix, y decrease
					arena.setCount(location);
					arena.setOccupy(location, i == cells[0]);
					arena.setArena(location);
				}
				// right
				for (int i = 1; i <= cells[1]; i++)
				{
					int[] location = new int[] { pos[0], pos[1] + i }; // x fix, y increase
					arena.setCount(location);
					arena.setOccupy(location, i == cells[1]);
					arena.setArena(location);
				}
				// front
				for (int i = 1; i <= cells[2]; i++)
				{
					int[] location = new int[] { pos[0] - i, pos[1] }; // y fix, x decrease
					arena.setCount(location);
					arena.setOccupy(location, i == cells[2]);
					arena.setArena(location);
				}
			}
		}
	}

	public void travelAnUnit()
	{
		int[] pos = arena.getPosition();
		int[] direct = arena.getDirection();
		if (direct[0] == 0)
		{ // N [0, 1] & S [0, -1]
			pilot.travel(Arena.UNIT_DEPTH);
			arena.setPosition(new int[] { pos[0], pos[1] + direct[1] });
		} else if (direct[1] == 0)
		{ // E [1, 0] & W [-1, 0]
			pilot.travel(Arena.UNIT_WIDTH);
			arena.setPosition(new int[] { pos[0] + direct[0], pos[1] });
		}
	}

	/**
	 * In the loop: 1. Scan around. 2. Update arena info. 3. Determine where to
	 * travel. (priority) 3.1. Move forward (in first round, check there always
	 * exists obstacles on the left) 3.2. Turn to unexplored area (-1 in arena
	 * matrix) 4. Travel an unit distance. 5. Update robot location and direction.
	 * 6. Update destination info (if detected).
	 */
	public void move()
	{
		// while (!Button.ESCAPE.isDown()) {
		for (int i = 0; i < 3; i++)
		{
			LCD.drawString(String.valueOf(arena.getPosition()[0]), 0, 2 * i);
			LCD.drawString(String.valueOf(arena.getPosition()[1]), 2, 2 * i);
			updateArenaInfo(scanAround());
			travelAnUnit();
		}
		writeInfo(arena.toString());
		// }
	}

	public static void main(String[] args) throws IOException
	{
		Explorer explorer = new Explorer();
		LCD.drawString("Press any button to start", 0, 0);
		Button.waitForAnyPress();
		LCD.clear();
		explorer.move();
		Button.waitForAnyPress();
		explorer.robot.closeRobot();
		System.exit(0);
	}

}
