// PilotMonitor.java
// 
// Based on the RobotMonitor class, this displays the robot
// state on the LCD screen; however, it works with the PilotRobot
// class that exploits a MovePilot to control the Robot.

// import java.text.DecimalFormat;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.GraphicsLCD;

public class PilotMonitor extends Thread
{
	private int delay;
	public PilotRobot robot;
	private String msg;
	private Arena arena;
	private volatile boolean running = true;

	GraphicsLCD lcd = LocalEV3.get().getGraphicsLCD();

	// Make the monitor a daemon and set
	// the robot it monitors and the delay
	public PilotMonitor(PilotRobot r, Arena arena, int d)
	{
		this.setDaemon(true);
		this.arena = arena;
		delay = d;
		robot = r;
		msg = "";

	}

	// Allow extra messages to be displayed
	public void resetMessage()
	{
		this.setMessage("");
	}

	// Clear the message that is displayed
	public void setMessage(String str)
	{
		msg = str;
	}

	// The monitor writes various bits of robot state to the screen, then
	// sleeps.
	public void run()
	{
		// The decimal format here is used to round the number to three significant
		// digits
		// DecimalFormat df = new DecimalFormat("####0.000");

		while (running)
		{
			if (Button.ESCAPE.isDown())
			{
				robot.getPilot().stop();
				this.stopRunning();
			}

			drawMap();

			try
			{
				sleep(delay);
			} catch (Exception e)
			{
				// We have no exception handling
			}
		}

		lcd.clear();
		lcd.setFont(Font.getDefaultFont());
		lcd.drawString("Finished Thread", 0, 20, 0);
	}

	public void stopRunning()
	{
		running = false;
	}

	public void drawMap()
	{
		double[][] map = arena.getArena();
		lcd.clear();
		for (int i = 0; i < map.length; i++)
		{
			for (int j = 0; j < map[i].length; j++)
			{
				if (map[i][j] <= Arena.THRESHOLD)
				{
					lcd.drawRect(10 + 15 * i, 10 + 15 * j, 15, 15);
				} else
				{
					lcd.fillRect(10 + 15 * i, 10 + 15 * j, 15, 15);
				}
			}
		}
	}

}
